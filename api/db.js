const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

let conn = null;
let db = null;

const init = async function(url, dbName, collectionName) {
  if (conn == null) {
    conn = await MongoClient.connect(url, {
      useNewUrlParser: true,
      poolSize: 5
    });
  }
  db = conn.db(dbName).collection(collectionName);
}

const close = () => {
  if (conn != null) {
    conn.close();
  }
}

const read = async function(id) {
  let res = await db.findOne({_id: new ObjectId(id)});
  return res;
}

module.exports = {
  init: init,
  close: close,
  read: read
}
