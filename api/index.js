const express = require("express");
const db = require("./db.js");
const elasticsearch = require('elasticsearch');
const { decorateApp } = require('@awaitjs/express');

require('dotenv').config({ path: '../.env' });

const es = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

let baseReq = {index: 'meds', type: 'med', size: 200}

db.init(process.env.MONGO_HOST, process.env.DB_NAME, process.env.COLLECTION_NAME);

const app = express();
app.set('json spaces', 2);
//const app = decorateApp(express());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
})

app.get('/meds/query', async function (req, res, next) {
  try {
    console.log("query: "+ JSON.stringify(req.query));
    let name = req.query.name;
    let generic = req.query.generic;
    let manufacturer = req.query.manufacturer;
    let ndc = req.query.ndc;

    let must = [];
    if (name) {
      console.log(name);
      must.push({match: {name: name}});
    };
    if (generic) {
      console.log(generic);
      must.push({match: {generic: generic}});
    };
    if (manufacturer) {
      console.log(manufacturer);
      must.push({match: {manufacturer: manufacturer}});
    };
    if (ndc) {
      console.log(ndc);
      must.push({match: {ndc: ndc}});
    };

    let esRes = await es.search(Object.assign(baseReq, {
      body: {
        query: {
          bool: {
            must: must
          }
        }
      }
    }));

    if (esRes.hits.total > 0) {
      res.json(esRes.hits.hits);
    } else {
      console.log("no results");
      res.json([]);
    }
  } catch (err) {
    res.status(500).json({error: err.stack});
    next(err);
  }
});

app.get('/meds/id/:medId', async function (req, res, next) {
  let med = await db.read(req.params.medId);
  console.log(med);
  res.json(med);
});

app.get('/meds/ndc/:ndc', async function (req, res, next) {
  try {
    let esRes = await es.search(Object.assign(baseReq, {
      body: {
        query: {
          match: {
            "ndc.keyword": req.params.ndc
          }
        }
      }
    }));
    if (esRes.hits.total > 0) {
      res.json(esRes.hits.hits);
    } else {
      console.log("no results");
      res.json([]);
    }
  } catch (err) {
    res.status(500).json({error: err.stack});
    next(err);
  }
});

app.get('/meds/name/:name', async function (req, res, next) {
  try {
    let esRes = await es.search(Object.assign(baseReq, {
      body: {
        query: {
          match: {
            name: req.params.name
          }
        }
      }
    }));
    if (esRes.hits.total > 0) {
      res.json(esRes.hits.hits);
    } else {
      console.log("no results");
      res.json([]);
    }
  } catch (err) {
    res.status(500).json({error: err.stack});
    next(err);
  }
});

app.get('/meds/setid/:setid', async function (req, res) {

  console.log(med);
  res.json(med);
});

app.get('/meds/search', async function (req, res) {

});

app.get('/meds/inactiveIngredient/:ingredientName', async function(req, res) {
  try {
    let esRes = await es.search(Object.assign(baseReq, {
      body: {
        query: {
          multi_match: {
            query: req.params.ingredientName,
            fields: ["inactiveIngredients"]
          }
        }
      }
    }));
    if (esRes.hits.total > 0) {
      res.json(esRes.hits.hits);
    } else {
      console.log("no results");
      res.json([]);
    }
  } catch (err) {
    res.status(500).json({error: err.stack});
    next(err);
  }
})

const port = process.env.PORT || 3001;

app.listen(port, () => {
  console.log("Server listening on port ", port);
})
