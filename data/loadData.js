const utils = require("./utils");
const util = require("util");
const path = require("path");
const _ = require("lodash");
const admZip = require("adm-zip");
const fs = require("fs");
const queue = require("queue");
const parseZip = require("./parseZip.js");
const db = require("./db.js");
const StreamZip = require('node-stream-zip');
const EventEmitter = require('events');


const loadData = async function (dataPath) {
  let conn;
  try {
    conn = await db.init();

    const bigQueue = queue({concurrency: 1, autostart: true});
    bigQueue.on('end', () => {
      conn.close();
    });
    bigQueue.on('success', res => {
      console.log(res);
    });

    const bigZips = fs.readdirSync(dataPath);

    bigZips.forEach(bigZip => {
      bigQueue.push(() => {
        console.log("Starting new bigzip job");
        return loadBigZip(conn, dataPath, bigZip);
      });
      console.log(bigQueue.length, " big queue jobs");
    });
  } catch (err) {
    console.log(err);
    if (conn != null) {
      conn.close();
    }
  }
}

const loadBigZip = async function(conn, dataPath, bigZip) {
  return new Promise((resolve, reject) => {
    try {
      console.log("Bigzip name: ", bigZip);
      const bigZipName = util.format("%s/%s", dataPath, bigZip);
      const zip = new StreamZip({
          file: bigZipName,
          storeEntries: true
      });

      console.log("Creating queue for bigZip ", bigZipName);
      let q = new ArchiveQueue(conn, bigZipName);
      q.on("done", () => {
        console.log("EMITTED DONE");
        resolve("Finished bigZip " + bigZip);
      });

      zip.on('entry', entry => {
          if (entry.name.endsWith(".zip")){
            const buffer = zip.entryDataSync(entry.name);
            //console.log("adding job to little q");
            q.push(entry.name, buffer);
          }
      });

      zip.on('ready', () => {
        console.log(q.length(), " jobs loaded.")
        q.start();
      });
    } catch (err) {
      return reject(err);
    }
  });
}




class ArchiveQueue extends EventEmitter  {
  constructor(conn, zipName) {
    super();
    this.conn = conn;
    this.zipName = zipName;
    this.q = queue({concurrency: 4 });

    this.q.on('success', (result, job) => {
      //console.log("Jobs remaining: ", this.q.length);
      console.log("Success: ", result);
    });
    this.q.on('error', (result, job) => {
      //console.log("Jobs remaining: ", this.q.length);
      //console.log("Job failed: ", result);
    });
    this.q.on('end', (err) => {
      if(err) {
        console.log("Queue for ", this.zipName, " finished with error ", err);
      } else {
        console.log("Queue for ", this.zipName, " finished successfully.");
      }
      this.emit("done");
    });
  }

  async addMeds (zipName, zipBuffer) {
    try {
      let drugObjs = parseZip(this.conn, zipName, zipBuffer);
      return db.insertRecs(this.conn, drugObjs);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  length() {
    return this.q.length;
  }

  start() {
    this.q.start();
  }

  push (zipName, zipBuffer) {
    this.q.push(() => {
      //console.log(`Jobs remaining: ${this.q.length}`);
      return this.addMeds(zipName, zipBuffer);
    });
  }
}



//for each inner zip
const loadArchive = async function (conn, zipName, zipBuffer) {
  try{

    const q = queue({concurrency: 4, autostart: true, });

    q.on('success', (result, job) => {
      console.log("Success: ", result);
    });
    q.on('error', (result, job) => {
      console.log("Job failed: ", result);
    });
    q.on('end', (err) => {
      if(err) {
        console.log("Queue for ", zipName, " finished with error ", err);
      } else {
        console.log("Queue for ", zipName, " finished successfully.");
      }
      db.close();
    });

    q.push(() => {
      let drugObjs = parseZip(conn, zipBuffer);
      return db.insertRecs(conn, drugObjs);
    });
  } catch (e) {
    console.log("Error: ", e);
  }

}

module.exports = loadData;
