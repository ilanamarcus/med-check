const utils = require("./utils");
const util = require("util");
const path = require("path");
const _ = require("lodash");
const admZip = require("adm-zip");
const fs = require("fs");
const cheer = require("cheerio");
const cheerioAdv = require('cheerio-advanced-selectors')
const cheerio = cheerioAdv.wrap(cheer);
const db = require("./db.js");

const NDC_CODE = "2.16.840.1.113883.6.69";

const parseFormCode = (formCode) => {
  return formCode.split(",").map(p=>p.trim()).reverse().join(" ");
}

//get zip file
const loadXml = (zipName, zipBuffer) => {
  const zip = new admZip(zipBuffer);
  const entries = zip.getEntries();
  const xmls = entries.filter(e => e.entryName.endsWith(".xml"));
  if (xmls.length == 0) {
    utils.log(path.basename(__filename),
      "File " + zipName + "has no xmls in file");
    return "";
  } else if (xmls.length > 1) {
    utils.log(path.basename(__filename),
      "File " + zipName + " has more than one xml file");
      return "";
  } else {
    return xmls[0].getData().toString('utf8');
  }
}

const getFileInfo = $ => {
  var setId = $("setId").attr("root");
  var manufacturer = $('author representedOrganization name').text();
  return {
    setId: setId,
    manufacturer: manufacturer
  }
};

const getBasicInfo = ($, med) => {
  //basic drug info
  var drugName = $("name:first", med).text();
  var form = $("formCode", med).attr("displayName");
  form = parseFormCode(form);

  var info = {
                name: drugName,
                form: form
              }
  var codeSystem = $("code", med).attr("codeSystem");
  var code = $("code", med).attr("code");
  if (NDC_CODE == codeSystem) {
    info.ndc = code;
  }
  return info;
};

const getActiveIngredients = ($, med) => {
  var actIngredients = [];
  var actIngredientsStrength = [];
  var activeIngredients = $("activeIngredient, ingredient[classCode^='ACTI']", med);
  if (activeIngredients.length == 0){
    console.log("No active ingredients for med " + drugName);
  } else {
    activeIngredients.each((x, ai) => {
      var quant = $("quantity", ai);
      var numerUnit = $("numerator", quant).attr("unit");
      var numerVal = $("numerator", quant).attr("value");
      var denomUnit = $("denominator", quant).attr("unit");
      var denomVal = $("denominator", quant).attr("value");

      var strength;
      if (denomUnit == undefined || denomUnit == 1) {
        strength = util.format("%d %s", numerVal, numerUnit);
      } else {
        strength = util.format("%d %s/%d %s", numerVal, numerUnit, denomVal, denomUnit);
      }

      actIngredientsStrength.push(strength)

      var subst = $("activeIngredientSubstance, ingredientSubstance", ai);
      var name = subst.children("name").text();
      actIngredients.push(name);
    });
  }
  return {
    activeIngredients: actIngredients,
    activeIngredientsStrength: actIngredientsStrength
  }
}

const getInactiveIngredients = ($, med) => {
  var inactIngredients = [];
  var inactiveIngredients = $("inactiveIngredient, ingredient[classCode='IACT']", med);
  inactiveIngredients.each((x, ii) => {
    var name = $("name", ii).text();
    inactIngredients.push(name);
  });
  return {
    inactiveIngredients: inactIngredients
  }
}

const getPackaging = ($, med) => {
  var packaging = [];
  var asContent = med.children("asContent");
  asContent.each((x, ac) => {
    var quantity = $("quantity numerator", ac).attr("value");
    var unit = $("quantity numerator", ac).attr("unit");
    var containerPackaged = $("containerPackagedMedicine, containerPackagedProduct", ac);
    var packageForm = $("formCode", containerPackaged).attr("displayName");

    var pack;
    if (unit == undefined || unit == 1) {
      unit = "unit";
    }
    pack = util.format("%d %s %s", quantity, unit, parseFormCode(packageForm));

    childAsContent = $(containerPackaged).children("asContent");
    if (childAsContent.length >0) {
      var childQuantity = $("quantity numerator", childAsContent[0]).attr("value");
      var childPackageForm = $("containerPackagedMedicine formCode, containerPackagedProduct formCode", childAsContent[0]).attr("displayName");
      pack = util.format("%s, %d per %s", pack, childQuantity, parseFormCode(childPackageForm));
    }

    var packInfo = {package: pack};

    var pkgCodeSystem = $("code", containerPackaged).attr("codeSystem");
    var pkgCode = $("code", containerPackaged).attr("code");
    if (NDC_CODE == pkgCodeSystem) {
      packInfo.ndc = pkgCode;
    }

    packaging.push(packInfo);
  });
  return {
    packaging: packaging
  }
}


const parseZip = function (conn, zipName, zipBuffer) {
  //let promise = new Promise((resolve, reject) => {
    //throw(new Error(zipName)); //for testing
    //try {
      console.log(util.format("Parsing file %s.", zipName));
      var xml = loadXml(zipName, zipBuffer);
      const $ = cheerio.load(xml, {xmlMode: true});
      const drugObjs = [];

      const drugs = $("component structuredBody component section subject");
      drugs.each((i, drug) => {
        let manufacturedProduct = $("manufacturedProduct", drug);
        let med = $("manufacturedProduct, manufacturedMedicine", manufacturedProduct);
        let obj = {};
        let origFile = {origFile: zipName};
        let fileInfo = getFileInfo($);
        let basicInfo = getBasicInfo($, med);
        let generic = {generic: $("asEntityWithGeneric name", med).text()};
        let activeIngredients = getActiveIngredients($, med);
        let inactiveIngredients = getInactiveIngredients($, med);
        let packaging = getPackaging($, med);
        obj = Object.assign(obj, origFile, fileInfo, basicInfo, generic, activeIngredients,
                inactiveIngredients, packaging);
        //console.log(obj);
        drugObjs.push(obj);
      });
      //resolve(util.format("File %s processed and sent to mongo.", filePath));
      return drugObjs;
    // } catch (err) {
    //     console.log(`Error on file ${zipName}`);
    //     throw(err);
    // }
  //return promise;
}

module.exports = parseZip;

//var filePath = process.argv[2];
//parseZip(filePath);
