const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const util = require("util");

let client = null;
const url = process.argv[2];
const dbName = process.argv[3]
const collectionName = process.argv[4]

// process.on('SIGINT', function() {
//   close();
// });

const init = async function () {
  if (client == null) {
    client = await MongoClient.connect(url, {
      useNewUrlParser: true,
      poolSize: 5
    });
  }
  return client;
}

const close = () =>{
  if (client != null){
    client.close()
      .then(()=>{
        console.log("Program terminated, closed db connection.");
      })
  } else {
    console.log("No db connection, program terminated.");
  }
}

const getDb = function (client) {
  let dbConn = null;
  try{
    dbConn = client.db(dbName);
  } catch (err) {
    console.log(err);
  }
  return dbConn;
}

const insertMany = async function (db, records) {
  const collection = db.collection(collectionName);
  let res = await collection.insertMany(records);
  assert.equal(records.length, res.ops.length);
  console.log(util.format("Inserted %d documents", res.ops.length));
  return res;
}

const insertRecs = async function (client, records) {
  let db = getDb(client);
  if (db == null) {
    console.log("Couldn't get connection to db.");
    return Promise.reject();
  }
  return insertMany(db, records);
}

module.exports = {
  init: init,
  close: close,
  insertRecs: insertRecs
}
