const mongoose = require('mongoose');
const mongoosastic = require('mongoosastic');

mongoose.connect("mongodb://localhost:27017/med-check", { useNewUrlParser: true }).then(
  () => { console.log("connected to db.") },
  err => { /** handle initial connection error */ }
);


let medSchema = new mongoose.Schema({
  setId: {type:String, es_indexed:true},
  manufacturer: {type:String, es_indexed:true},
  name: {type:String, es_indexed:true},
  form: {type:String,es_indexed:false},
  ndc: {type:String, es_indexed:true},
  generic: {type:String, es_indexed:true},
  activeIngredients: {type:[String], es_indexed:true},
  inactiveIngredients: {type: [String], es_indexed:true},
  packaging: {type:[{package: String, ndc: String}], es_indexed:true}
});

medSchema.plugin(mongoosastic);

let Med = mongoose.model("Med", medSchema);

var count = 0;
var stream = Med.synchronize();

stream.on('data', function(err, doc){
  if(err) {
    console.log(err);
  } else if (!doc) {
    console.log("data is empty");
  } else {
    console.log(doc);
    count++;
  }
});
stream.on('close', function(){
  console.log('DONE indexed ' + count + ' documents!');
});
stream.on('error', function(err){
  console.log(err);
});
