
const log = (moduleName,  message) => {
  console.log(moduleName + ": " + message);
};

module.exports = {log: log};
