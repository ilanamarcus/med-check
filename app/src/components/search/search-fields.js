import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import {connect} from 'react-redux';
import {setName, setManufacturer, setGeneric, setNDC} from '../../redux/actions.js';

class SearchFields extends React.Component {
  nameChange = e => {
    this.props.setName(e.target.value);
  };

  genericChange = e => {
    this.props.setGeneric(e.target.value);
  };

  manufacturerChange = e => {
    this.props.setManufacturer(e.target.value);
  };

  ndcChange = e => {
    this.props.setNDC(e.target.value);
  };

  render(){
    return (
       <React.Fragment>
      <Grid container spacing={24}>
        <Grid item xs={12} md={6}>
          <TextField id="brandName" label="Name of medication" fullWidth onBlur={this.nameChange}/>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField id="genericName" label="Name of generic (active ingredient)" fullWidth onBlur={this.genericChange}/>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField id="manufacturer" label="Manufacturer" fullWidth onBlur={this.manufacturerChange}/>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            id="ndc"
            label="NDC"
            fullWidth
            onBlur={this.ndcChange}
          />
        </Grid>
      </Grid>
    </React.Fragment>
    )}
}


let mapDispatch = {
  setName: setName, 
  setManufacturer: setManufacturer,
  setGeneric: setGeneric,
  setNDC: setNDC
}

export default connect(null, mapDispatch)(SearchFields);
