import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import DrugPanel from './drug-panel.js';
import CircularProgress from '@material-ui/core/CircularProgress';


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  chip: {
    margin: theme.spacing.unit*.25,
    color: "secondary"
  },
  spinner: {
    color: "secondary"
  }
});

function SearchResults(props) {
  let { classes, meds, loading, error } = props;
  let drugs = meds.map((m,i) =>
    <DrugPanel med={m} />
  );

  let getResults = () => {
    if (loading) {
        return <CircularProgress />;
    } else if (error.hasError) {
        return error.msg;
    } else if (meds.length<1){
        return "No results.";
    } else {
      return drugs;
    }
  }

  return (
    <div className={classes.root}>
      { getResults()}
    </div>
  );
}

SearchResults.propTypes = {
  classes: PropTypes.object.isRequired,
  meds: PropTypes.array.isRequired
};

export default withStyles(styles)(SearchResults);
