import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import SearchMed from './search-med';
import SearchResults from './search-results';

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 1,
    marginRight: theme.spacing.unit * 1,
    [theme.breakpoints.up(800 + theme.spacing.unit * 2 * 2)]: {
      width: 800,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 1,
    [theme.breakpoints.up(800 + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3,
    },
  }
});


class SearchContainer extends React.Component {
  render() {
    let { classes, meds, loading, showResults, error } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          <Paper className={classes.paper}>
              <SearchMed />
          </Paper>
          {
            !showResults
              ? null
              :
                <Paper className={classes.paper}>
                  <SearchResults meds={meds} loading={loading} error={error}/>
                </Paper>
          }
        </main>
      </React.Fragment>
    );
  }
}

SearchContainer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SearchContainer);
