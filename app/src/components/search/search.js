import React, {Component} from 'react';
import SearchContainer from './search-container.js';
import {connect} from 'react-redux';

class Search extends Component {

  render() {
    let {meds, loading, showResults, error} = this.props;
    return (
      <SearchContainer meds={meds} loading={loading} showResults={showResults}
        error={error}/>
    );
  }
}

let mapState = state => {
  console.log("how many meds " + state.meds.length)
  console.log("their names are " + state.meds.map(m=> m.name).join())
  return {
    meds: state.meds,
    loading: state.loading,
    showResults: state.showResults,
    error: state.error
  };
}

export default connect(mapState)(Search);
