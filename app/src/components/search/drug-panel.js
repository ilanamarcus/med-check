import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  chip: {
    margin: theme.spacing.unit*.25,
    fontSize: theme.typography.pxToRem(10)
  }, 
  inactiveIng: {
    marginBottom: theme.spacing.unit
  }
});

function DrugPanel(props) {
  let {classes, med} = props;
  console.log("IN drug panel! what is med name? "+ med.name);
  return (
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Grid container spacing={16}
          direction="row"
          justify="flex-start"
          alignItems="flex-start">
          <Grid item align="left" xs={12} md={4}>
            <Typography variant="body2">{med.name}</Typography>
            <Typography variant="caption">{med.generic}</Typography>
          </Grid>
          <Grid item align="left" xs={12} md={3}>
            <Typography variant="body2">{med.ndc}</Typography>
            <Typography variant="caption">NDC</Typography>
          </Grid>
          <Grid item align="left" xs={12} md={5}>
            <Typography variant="body2">{med.manufacturer}</Typography>
            <Typography variant="caption">
              { med.packaging.map((p,i) =>
                <Button size="small" className={classes.chip} variant="outlined">{p.package}</Button>
              )}
            </Typography>
          </Grid>
        </Grid>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Grid container spacing={16}
          direction="row"
          justify="flex-start"
          alignItems="flex-start">
          <Grid item align="left" xs={12} md={12} >
            <Typography variant="caption" className={classes.inactiveIng}>
                {med.inactiveIngredients.length>0?"Inactive ingredients":"No inactive ingredients available."}
              </Typography>
              {med.inactiveIngredients.map((ia,i) =>
                <Chip label={ia} className={classes.chip} color="secondary"/>
              )}
          </Grid>
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

DrugPanel.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DrugPanel);
