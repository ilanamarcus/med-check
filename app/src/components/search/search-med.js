import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Button from '@material-ui/core/Button';
import SearchFields from './search-fields';
import {doSearch} from '../../redux/middleware.js';
import {connect} from 'react-redux';



const styles = theme => ({
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit,
  },
});


class SearchMed extends React.Component {
  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <SearchFields />
        <div className={classes.buttons}>
          <Button
            variant="contained"
            color="primary"
            onClick={this.props.search}
            className={classes.button}
          >
            Search
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

SearchMed.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = {
  search: doSearch
}

SearchMed = connect(null, mapDispatchToProps)(SearchMed);
export default withStyles(styles)(SearchMed);
