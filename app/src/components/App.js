import React, { Component } from 'react';
import Search from './search/search.js';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import withStyles from '@material-ui/core/styles/withStyles';



const styles = theme => ({
  appBar: {
    position: 'relative'
  }, 
  grow: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing.unit
  }
})

class App extends Component {
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <div className="App">
          <AppBar position="static" color="default" className={classes.appBar}>
            <Toolbar>
              <Typography variant="h6" color="inherit" className={classes.grow} align="left" noWrap>
                What's in my meds
              </Typography>
              <Typography>
              <Link href="http://www.ilanarmarcus.com" target="_blank" className={classes.link}>
                Who built this?
              </Link>
              </Typography>
            </Toolbar>
          </AppBar>
          <Search />
        </div>
      </React.Fragment>
    );
  }
}


export default withStyles(styles)(App);
