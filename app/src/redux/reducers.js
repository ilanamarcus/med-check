import {combineReducers} from 'redux';
import {Actions} from './actions.js';

const name = (state = "", action) => {
  if (action.type === Actions.SET_NAME) {
    const val = action.val;
    return val;
  }
  return state;
};

const generic = (state = "", action) => {
  if (action.type === Actions.SET_GENERIC) {
    const val = action.val;
    return val;
  }
  return state;
};

const manufacturer = (state = "", action) => {
  if (action.type === Actions.SET_MANUFACTURER) {
    const val = action.val;
    return val;
  }
  return state;
};

const ndc = (state = "", action) => {
  if (action.type === Actions.SET_NDC) {
    const val = action.val;
    return val;
  }
  return state;
};

const loading = (state = false, action) => {
  if (action.type === Actions.LOADING_SEARCH) {
    return true;
  } else if (action.type === Actions.SEARCH_DONE) {
    return false;
  }
  return state;
}

const showResults = (state = false, action) => {
  if (action.type === Actions.SHOW_RESULTS) {
    return true;
  }
  return state;
}

const meds = (state=[], action) => {
  if (action.type === Actions.UPDATE_MEDS) {
    return action.val;
  } else if (action.type === Actions.CLEAR_MEDS) {
    return [];
  }
  return state;
}

const error = (state={hasError: false}, action) => {
  if (action.type === Actions.ERROR) {
    return {
      hasError: action.val.hasError,
      msg: action.val.msg
    }
  }
  return state;
}

export default combineReducers({
  name, generic, manufacturer, ndc, loading, meds, showResults, error
});
