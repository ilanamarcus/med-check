export const Actions = {
 SET_NAME: "SET_NAME",
 SET_GENERIC: "SET_GENERIC",
 SET_MANUFACTURER: "SET_MANUFACTURER",
 SET_NDC: "SET_NDC",
 LOADING_SEARCH: "LOADING_SEARCH",
 SEARCH_DONE: "SEARCH_DONE",
 UPDATE_MEDS: "UPDATE_MEDS",
 CLEAR_MEDS: "CLEAR_MEDS",
 SHOW_RESULTS: "SHOW_RESULTS",
 ERROR: "ERROR"
}

export const setName = text => ({
  type: Actions.SET_NAME,
  val: text
});

export const setGeneric = text => ({
  type: Actions.SET_GENERIC,
  val: text
});

export const setManufacturer = text => ({
  type: Actions.SET_MANUFACTURER,
  val: text
});

export const setNDC = text => ({
  type: Actions.SET_NDC,
  val: text
});

export const updateMeds = meds => ({
  type: Actions.UPDATE_MEDS,
  val: meds
});

export const clearMeds = () => ({
  type: Actions.CLEAR_MEDS
});

export const loadingSearch = () => ({
  type: Actions.LOADING_SEARCH
});

export const searchDone = () => ({
  type: Actions.SEARCH_DONE
});

export const showResults = () => ({
  type: Actions.SHOW_RESULTS
});

export const error = err => ({
  type: Actions.ERROR,
  val: {
    hasError: err.hasError,
    msg: err.msg
  }
})
