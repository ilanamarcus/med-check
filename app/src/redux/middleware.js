import {loadingSearch, searchDone, updateMeds, clearMeds, showResults, error} from './actions.js';
const fetch = require("node-fetch");
const queryString = require("query-string");

const searchMeds = query => {
  let url = process.env.REACT_APP_API_HOST+"/meds/query?"+queryString.stringify(query);
  console.log(url);
  const promise = fetch(url);
  return promise;
}

const buildQuery = (name, generic, manufacturer, ndc) => {
  let match = {};
  if (name) {match.name = name};
  if (generic) {match.generic = generic};
  if (manufacturer) {match.manufacturer = manufacturer};
  if (ndc) {match.ndc = ndc};

  console.log("match "+JSON.stringify(match));
  return match;
}
export const doSearch = () => {
  return async (dispatch, getState) => {
    console.log("clicked search");
    try{
      dispatch(loadingSearch());
      dispatch(error({hasError: false, msg: ""}));
      dispatch(clearMeds());
      dispatch(showResults())
      let state = getState();
      let query = buildQuery(state.name, state.generic, state.manufacturer, state.ndc);
      let res = await searchMeds(query).then(r=>r.json());
      let meds = res.map(m=>m._source);
      dispatch(updateMeds(meds));
      dispatch(searchDone());
    } catch (err) {
      console.log(err.message);
      dispatch(error({hasError: true, msg: err.message + err.stack}));
      dispatch(searchDone());
    }
  }
}
